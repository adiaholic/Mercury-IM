package org.mercury_im.messenger.core;


import org.mercury_im.messenger.persistence.util.ChatAndPossiblyContact;

public interface NotificationManager {

    int chatMessageReceived(ChatAndPossiblyContact chatAndPossiblyContact, String body);

}
