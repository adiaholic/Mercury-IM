package org.mercury_im.messenger.core.centers;

import javax.inject.Inject;

public class MessageCenter {

    private final ConnectionCenter connectionCenter;

    @Inject
    public MessageCenter(ConnectionCenter connectionCenter) {
        this.connectionCenter = connectionCenter;
    }
}
