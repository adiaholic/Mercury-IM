package org.mercury_im.messenger.core.util;

import org.mercury_im.messenger.persistence.entity.ContactModel;
import org.mercury_im.messenger.persistence.entity.EntityModel;

public class ContactNameUtil {

    public static String displayableNameFrom(ContactModel contactModel) {
        if (contactModel == null) {
            return null;
        }

        if (contactModel.getRostername() != null) {
            return contactModel.getRostername();
        }
        if (contactModel.getEntity() != null) {
            return contactModel.getEntity().getJid().getLocalpart().asUnescapedString();
        }

        return null;
    }

    public static String displayableNameFrom(ContactModel contact, EntityModel entity) {
        if (contact == null) {
            return entity.getJid().getLocalpart().asUnescapedString();
        } return displayableNameFrom(contact);
    }
}
