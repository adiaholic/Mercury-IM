package org.mercury_im.messenger.core.connection;

/**
 * {@link MercuryConnection} modeled as a finite state machine.
 * Below enums represent the states of the machine.
 */
public enum ConnectionState {

    /**
     * Connection is disconnected.
     * This is the initial state of the machine.
     */
    DISCONNECTED,

    /**
     * The connection is in the process of connecting to the server.
     * This state can be reached by issuing a connect() call from within the {@link #DISCONNECTED}
     * state.
     */
    CONNECTING,

    /**
     * The connection is successfully connected to the server and the stream has been initiated.
     */
    CONNECTED,

    /**
     * THe connection is authenticated.
     * In this state the connection is ready to send and receive stanzas.
     */
    AUTHENTICATED,

    /**
     * The connection is in the process of shutting down.
     */
    DISCONNECTING,

    /**
     * The device doesn't have usable network connectivity.
     */
    WAITING_FOR_NETWORK,

    /**
     * The connection already (unsuccessfully) tried to connect, but failed due to lack of network
     * connectivity and is now waiting to retry connecting.
     */
    WAITING_FOR_RETRY
}
