package org.mercury_im.messenger.core.di;

import org.mercury_im.messenger.persistence.di.RequeryModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        CenterModule.class,
        RequeryModule.class
})
public interface XmppComponent {

}
