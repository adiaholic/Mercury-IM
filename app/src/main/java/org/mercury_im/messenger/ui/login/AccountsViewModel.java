package org.mercury_im.messenger.ui.login;

import android.app.Application;
import android.widget.Toast;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import org.mercury_im.messenger.MercuryImApplication;
import org.mercury_im.messenger.core.centers.ConnectionCenter;
import org.mercury_im.messenger.core.connection.MercuryConnection;
import org.mercury_im.messenger.persistence.entity.AccountModel;
import org.mercury_im.messenger.persistence.repository.AccountRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class AccountsViewModel extends AndroidViewModel {

    @Inject
    AccountRepository repository;

    @Inject
    ConnectionCenter connectionCenter;

    private final MutableLiveData<List<AccountModel>> accounts = new MutableLiveData<>();
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    public AccountsViewModel(Application application) {
        super(application);
        MercuryImApplication.getApplication().getAppComponent().inject(this);
        compositeDisposable.add(repository.getAll()
                .subscribe(accountModels -> accounts.setValue(accountModels.toList())));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.clear();
    }

    public LiveData<List<AccountModel>> getAccounts() {
        return accounts;
    }

    public void toggleAccountEnabled(AccountModel accountModel) {
        MercuryConnection connection = connectionCenter.getConnection(accountModel);
        if (connection == null) {
            Toast.makeText(this.getApplication(), "MercuryConnection is null!", Toast.LENGTH_LONG).show();
            return;
        }

        accountModel.setEnabled(!accountModel.isEnabled());
        repository.upsert(accountModel)
                .subscribe();
    }
}
