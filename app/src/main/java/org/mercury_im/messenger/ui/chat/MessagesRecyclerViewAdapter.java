package org.mercury_im.messenger.ui.chat;

import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.mercury_im.messenger.R;
import org.mercury_im.messenger.persistence.entity.MessageModel;
import org.mercury_im.messenger.ui.util.MessageBackgroundDrawable;

import java.util.ArrayList;
import java.util.List;

public class MessagesRecyclerViewAdapter extends RecyclerView.Adapter<MessagesRecyclerViewAdapter.MessageViewHolder> {

    private List<MessageModel> messages = new ArrayList<>();
    private SparseArray<Boolean> checkedItems = new SparseArray<>();

    public MessagesRecyclerViewAdapter() {

    }

    public void updateMessages(List<MessageModel> messages) {
        this.messages.clear();
        this.messages.addAll(messages);
        notifyDataSetChanged();
    }

    public void setItemChecked(int position, boolean checked) {
        checkedItems.put(position, checked);
    }

    public boolean isItemChecked(int position) {
        return checkedItems.get(position, false);
    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MessageViewHolder(LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MessageViewHolder holder, int position) {
        MessageModel message = messages.get(position);
        MessageBackgroundDrawable background = getBackgroundDrawable(position);
        if (message.isIncoming()) {
            holder.body.setBackgroundResource(background.getIncomingDrawable());
        } else {
            holder.body.setBackgroundResource(background.getOutgoingDrawable());
        }
        holder.body.setText(message.getBody());
        holder.body.requestLayout();
    }

    @Override
    public int getItemViewType(int position) {
        if (messages.get(position).isIncoming()) {
            return R.layout.view_message_text_in;
        } else {
            return R.layout.view_message_text_out;
        }
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public class MessageViewHolder extends RecyclerView.ViewHolder {

        private TextView body;

        public MessageViewHolder(@NonNull View itemView) {
            super(itemView);

            body = itemView.findViewById(R.id.msg_body);
        }
    }

    public MessageBackgroundDrawable getBackgroundDrawable(int pos) {
        MessageModel subject = messages.get(pos);
        MessageModel predecessor = pos != 0 ? messages.get(pos - 1) : null;
        MessageModel successor = pos != messages.size() - 1 ? messages.get(pos + 1) : null;

        if (predecessor == null || predecessor.isIncoming() != subject.isIncoming()) {
            if (successor == null || successor.isIncoming() != subject.isIncoming()) {
                // This is a single message
                return MessageBackgroundDrawable.SINGLE;
            } else {
                // This is the first message of a group
                return MessageBackgroundDrawable.FIRST;
            }
        } else {
            if (successor == null || successor.isIncoming() != subject.isIncoming()) {
                // This is the last message of a group
                return MessageBackgroundDrawable.LAST;
            } else {
                // This message is in the middle of a group
                return MessageBackgroundDrawable.MID;
            }
        }
    }
}
