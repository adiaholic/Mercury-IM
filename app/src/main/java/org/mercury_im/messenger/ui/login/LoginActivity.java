package org.mercury_im.messenger.ui.login;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.textfield.TextInputEditText;

import org.mercury_im.messenger.MercuryImApplication;
import org.mercury_im.messenger.R;
import org.mercury_im.messenger.persistence.entity.AccountModel;
import org.mercury_im.messenger.util.TextChangedListener;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements TextView.OnEditorActionListener {

    // UI references.
    @BindView(R.id.jid)
    TextInputEditText mJidView;

    @BindView(R.id.password)
    TextInputEditText mPasswordView;

    @BindView(R.id.sign_in_button)
    Button mSignInView;

    private LoginViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        MercuryImApplication.getApplication().getAppComponent().inject(this);

        // Set up the login form.
        ButterKnife.bind(this);

        viewModel = new ViewModelProvider(this).get(LoginViewModel.class);

        observeViewModel(viewModel);
        displayCredentials(viewModel.getAccount());

        mJidView.setOnEditorActionListener(this);
        mPasswordView.setOnEditorActionListener(this);

        mJidView.addTextChangedListener(new TextChangedListener() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                viewModel.onJidInputChanged(charSequence.toString());
                Log.d("Mercury", "onTextChanged");
            }
        });

        mPasswordView.addTextChangedListener(new TextChangedListener() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                viewModel.onPasswordInputChanged(charSequence.toString());
                Log.d("Mercury", "onTextChanged");
            }
        });

        mSignInView.setOnClickListener(view -> viewModel.loginDetailsEntered());
    }

    private void observeViewModel(LoginViewModel viewModel) {
        viewModel.getJidError().observe(this, jidError -> {
            if (jidError == null) return;
            String errorMessage = null;
            switch (jidError) {
                case none:
                    break;
                case emptyJid:
                    errorMessage = getResources().getString(R.string.error_field_required);
                    break;
                case invalidJid:
                    errorMessage = getResources().getString(R.string.error_invalid_jid);
                    break;
                case unknownJid:
                    errorMessage = "Unknown Jid!";
            }
            mJidView.setError(errorMessage);
        });

        viewModel.getPasswordError().observe(this, passwordError -> {
            if (passwordError == null) return;
            String errorMessage = null;
            switch (passwordError) {
                case none:
                    break;
                case emptyPassword:
                    errorMessage = getResources().getString(R.string.error_field_required);
                    break;
                case invalidPassword:
                    errorMessage = getResources().getString(R.string.error_invalid_password);
                    break;
                case incorrectPassword:
                    errorMessage = getResources().getString(R.string.error_incorrect_password);
                    break;
            }
            mPasswordView.setError(errorMessage);
        });

        viewModel.getSigninSuccessful().observe(this, aBoolean -> {
            if (Boolean.TRUE.equals(aBoolean)) {
                finish();
            }
        });
    }

    private void displayCredentials(LiveData<AccountModel> account) {
        account.observe(this, accountModel -> {
            if (accountModel == null) {
                return;
            }

            if (accountModel.getJid() != null) {
                mJidView.setText(accountModel.getJid().toString());
            }

            if (accountModel.getPassword() != null) {
                mPasswordView.setText(accountModel.getPassword());
            }
        });
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        switch (v.getId()) {
            case R.id.jid:
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    mPasswordView.requestFocus();
                    return true;
                }
                break;

            case R.id.password:
                if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_NULL) {
                    viewModel.loginDetailsEntered();
                    return true;
                }
        }
        return false;
    }
}
