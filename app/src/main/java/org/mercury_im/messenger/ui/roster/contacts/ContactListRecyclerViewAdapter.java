package org.mercury_im.messenger.ui.roster.contacts;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jxmpp.jid.EntityBareJid;
import org.mercury_im.messenger.R;
import org.mercury_im.messenger.persistence.entity.ContactModel;
import org.mercury_im.messenger.ui.chat.ChatActivity;
import org.mercury_im.messenger.ui.util.AbstractRecyclerViewAdapter;
import org.mercury_im.messenger.util.ColorUtil;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactListRecyclerViewAdapter
        extends AbstractRecyclerViewAdapter<ContactModel, ContactListRecyclerViewAdapter.RosterItemViewHolder> {

    public ContactListRecyclerViewAdapter() {
        super(new ContactDiffCallback());
    }

    @NonNull
    @Override
    public RosterItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RosterItemViewHolder(parent.getContext(), LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_contact, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RosterItemViewHolder holder, int position) {
        ContactModel model = getModelAt(position);
        holder.bind(model);
    }

    public class RosterItemViewHolder extends RecyclerView.ViewHolder {

        private View view;

        @BindView(R.id.roster_entry__jid)
        TextView jidView;

        @BindView(R.id.roster_entry__name)
        TextView nameView;

        @BindView(R.id.roster_entry__avatar)
        ImageView avatarView;

        Context context;

        public RosterItemViewHolder(Context context, View itemView) {
            super(itemView);
            this.context = context;
            this.view = itemView;
            ButterKnife.bind(this, view);
        }

        void bind(ContactModel contactModel) {
            String name = contactModel.getRostername();
            nameView.setText(name != null ? name : contactModel.getEntity().getJid().getLocalpart().asUnescapedString());
            EntityBareJid jid = contactModel.getEntity().getJid();
            jidView.setText(jid.toString());
            avatarView.setColorFilter(ColorUtil.consistentColor(jid.toString()));
            view.setOnClickListener(view -> {

                Intent intent = new Intent(context, ChatActivity.class);
                intent.putExtra(ChatActivity.EXTRA_JID, jid.toString());
                intent.putExtra(ChatActivity.EXTRA_ACCOUNT, contactModel.getEntity().getAccount().getId());

                context.startActivity(intent);
            });
        }
    }

    private static class ContactDiffCallback extends AbstractDiffCallback<ContactModel> {

        ContactDiffCallback() {
            super(true);
        }

        @Override
        public boolean areItemsTheSame(ContactModel oldItem, ContactModel newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(ContactModel oldItem, ContactModel newItem) {
            return areItemsTheSame(oldItem, newItem) &&
                    Objects.equals(oldItem.getRostername(), newItem.getRostername());
        }
    }
}
