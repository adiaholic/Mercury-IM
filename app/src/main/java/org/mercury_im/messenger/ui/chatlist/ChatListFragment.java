package org.mercury_im.messenger.ui.chatlist;

import static org.mercury_im.messenger.MercuryImApplication.TAG;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;

import butterknife.BindView;
import butterknife.ButterKnife;

import org.mercury_im.messenger.R;

public class ChatListFragment extends Fragment {

    private ChatListViewModel viewModel;

    @BindView(R.id.chat_list__recycler_view)
    RecyclerView recyclerView;
    private final ChatListRecyclerViewAdapter recyclerViewAdapter = new ChatListRecyclerViewAdapter();

    @BindView(R.id.fab)
    ExtendedFloatingActionButton fab;

    public ChatListFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat_list, container, false);
        ButterKnife.bind(this, view);

        recyclerView.setAdapter(recyclerViewAdapter);

        fab.setOnClickListener(v -> Toast.makeText(getContext(), R.string.not_yet_implemented, Toast.LENGTH_SHORT).show());
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        viewModel = ViewModelProviders.of(getActivity()).get(ChatListViewModel.class);
        viewModel.getChats().observe(this, chatModels -> {
            if (chatModels == null) {
                Log.d(TAG, "Displaying null chats");
                return;
            }
            Log.d(TAG, "Displaying " + chatModels.size() + " chats");
            recyclerViewAdapter.setModels(chatModels);
        });
    }
}
