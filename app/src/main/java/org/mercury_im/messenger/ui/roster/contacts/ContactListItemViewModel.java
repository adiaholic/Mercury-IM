package org.mercury_im.messenger.ui.roster.contacts;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import org.mercury_im.messenger.persistence.entity.ContactModel;
import org.mercury_im.messenger.persistence.repository.RosterRepository;

import javax.inject.Inject;

public class ContactListItemViewModel extends AndroidViewModel {

    @Inject
    RosterRepository contactRepository;

    private LiveData<ContactModel> contact;

    @Inject
    public ContactListItemViewModel(@NonNull Application application) {
        super(application);
    }

}
