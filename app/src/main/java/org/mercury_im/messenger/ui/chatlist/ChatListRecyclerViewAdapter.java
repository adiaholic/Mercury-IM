package org.mercury_im.messenger.ui.chatlist;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.recyclerview.widget.RecyclerView;

import org.mercury_im.messenger.R;
import org.mercury_im.messenger.persistence.entity.ChatModel;
import org.mercury_im.messenger.ui.chat.ChatActivity;
import org.mercury_im.messenger.ui.util.AbstractRecyclerViewAdapter;
import org.mercury_im.messenger.util.ColorUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatListRecyclerViewAdapter
        extends AbstractRecyclerViewAdapter<ChatModel, ChatListRecyclerViewAdapter.ChatHolder> {

    public ChatListRecyclerViewAdapter() {
        super(new ChatMessageDiffCallback(true));
    }

    @NonNull
    @Override
    public ChatHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_chat, parent, false);
        return new ChatHolder(parent.getContext(), view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatHolder holder, int position) {
        ChatModel model = getModelAt(position);
        holder.nameView.setText(model.getPeer().getJid() != null ?
                model.getPeer().getJid() : model.toString());
        holder.avatarView.setColorFilter(ColorUtil.consistentColor(model.getPeer().getJid().toString()));

        holder.itemView.setOnClickListener(view -> {

            Intent intent = new Intent(holder.context, ChatActivity.class);
            intent.putExtra(ChatActivity.EXTRA_JID, model.getPeer().getJid().toString());
            intent.putExtra(ChatActivity.EXTRA_ACCOUNT, model.getPeer().getAccount().getId());

            holder.context.startActivity(intent);
        });

        holder.itemView.setOnLongClickListener(v -> {
            ((AppCompatActivity) v.getContext()).startSupportActionMode(actionModeCallback);
            return true;
        });
    }

    public static class ChatHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.chat_name)
        TextView nameView;

        @BindView(R.id.chat_message)
        TextView chatMessage;

        @BindView(R.id.chat_time)
        TextView chatTime;

        @BindView(R.id.chat_avatar)
        ImageView avatarView;

        Context context;

        public ChatHolder(Context context, @NonNull View itemView) {
            super(itemView);
            this.context = context;
            ButterKnife.bind(this, itemView);
        }
    }

    private static class ChatMessageDiffCallback extends AbstractDiffCallback<ChatModel> {

        ChatMessageDiffCallback(boolean detectMoves) {
            super(detectMoves);
        }

        @Override
        public boolean areItemsTheSame(ChatModel oldItem, ChatModel newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(ChatModel oldItem, ChatModel newItem) {
            return areItemsTheSame(oldItem, newItem);
        }
    }

    private androidx.appcompat.view.ActionMode.Callback actionModeCallback = new androidx.appcompat.view.ActionMode.Callback() {
        private boolean multiSelect = false;

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.actionmode_chatlist, menu);
            multiSelect = true;
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

            mode.finish();

            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            notifyDataSetChanged();
        }
    };
}
