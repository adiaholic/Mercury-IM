package org.mercury_im.messenger.ui.roster.contacts;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.mercury_im.messenger.MercuryImApplication;
import org.mercury_im.messenger.persistence.entity.ContactModel;
import org.mercury_im.messenger.persistence.repository.RosterRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


public class ContactListViewModel extends ViewModel {

    @Inject
    RosterRepository rosterRepository;

    private final MutableLiveData<List<ContactModel>> rosterEntryList = new MutableLiveData<>();
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    public ContactListViewModel() {
        super();
        MercuryImApplication.getApplication().getAppComponent().inject(this);
        Log.d("ContactListViewModel", "Start observing database");
        // Subscribe to changes to the contacts table and update the LiveData object for the UI.
        compositeDisposable.add(rosterRepository.getAllContacts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    List<ContactModel> list = o.toList();
                    Log.d("ContactListViewModel", "Room changed contacts: " + list.size());
                    rosterEntryList.setValue(list);
                }));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.clear();
    }

    public LiveData<List<ContactModel>> getRosterEntryList() {
        return rosterEntryList;
    }
}
