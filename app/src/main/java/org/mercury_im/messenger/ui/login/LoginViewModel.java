package org.mercury_im.messenger.ui.login;

import static org.mercury_im.messenger.core.connection.MercuryConnection.TAG;

import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import io.reactivex.Single;
import io.reactivex.observers.DisposableSingleObserver;

import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.mercury_im.messenger.MercuryImApplication;
import org.mercury_im.messenger.core.centers.ConnectionCenter;
import org.mercury_im.messenger.persistence.entity.AccountModel;
import org.mercury_im.messenger.persistence.repository.AccountRepository;

import javax.inject.Inject;

public class LoginViewModel extends ViewModel {

    @Inject
    AccountRepository accountRepository;

    @Inject
    ConnectionCenter connectionCenter;

    private String jid;
    private String password;

    private MutableLiveData<JidError> jidError = new MutableLiveData<>();
    private MutableLiveData<PasswordError> passwordError = new MutableLiveData<>();

    private MutableLiveData<AccountModel> account = new MutableLiveData<>();

    private MutableLiveData<Boolean> signinSuccessful = new MutableLiveData<>();

    public LoginViewModel() {
        super();
        MercuryImApplication.getApplication().getAppComponent().inject(this);
        init(new AccountModel());
    }

    public LiveData<Boolean> getSigninSuccessful() {
        return signinSuccessful;
    }

    public enum JidError {
        none,
        emptyJid,
        invalidJid,
        unknownJid
    }

    public enum PasswordError {
        none,
        emptyPassword,
        invalidPassword,
        incorrectPassword
    }

    public void onJidInputChanged(String input) {
        this.jid = input;

    }

    public void onPasswordInputChanged(String input) {
        this.password = input;
    }

    public LiveData<JidError> getJidError() {
        return jidError;
    }

    public LiveData<PasswordError> getPasswordError() {
        return passwordError;
    }

    /**
     * Try to parse the input string into a {@link EntityBareJid} and return it.
     * Return null on failure.
     * @param input input string
     * @return valid jid or null
     */
    private EntityBareJid asValidJidOrNull(String input) {
        return JidCreate.entityBareFromOrNull(input);
    }

    private boolean isPasswordValid(String password) {
        return !password.isEmpty();
    }

    public void init(@NonNull AccountModel account) {
        this.account.setValue(account);
    }

    public MutableLiveData<AccountModel> getAccount() {
        return account;
    }

    public void login() {
        AccountModel account = getAccount().getValue();
        if (account != null && account.getJid() != null && !TextUtils.isEmpty(account.getPassword())) {
            accountRepository.upsert(account);
        }
    }

    public void loginDetailsEntered() {
        boolean loginIntact = true;
        if (jid.isEmpty()) {
            jidError.postValue(JidError.emptyJid);
            loginIntact = false;
        }

        EntityBareJid bareJid = asValidJidOrNull(jid);
        if (bareJid == null) {
            jidError.postValue(JidError.invalidJid);
            loginIntact = false;
        }

        if (!isPasswordValid(password)) {
            passwordError.postValue(PasswordError.invalidPassword);
            loginIntact = false;
        }

        if (loginIntact) {
            AccountModel accountModel = new AccountModel();
            accountModel.setEnabled(true);
            accountModel.setJid(bareJid);
            accountModel.setPassword(password);
            Single<AccountModel> insert = accountRepository.upsert(accountModel);
            insert.subscribe(new DisposableSingleObserver<AccountModel>() {
                        @Override
                        public void onSuccess(AccountModel inserted) {
                            Log.d(MercuryImApplication.TAG, "LoginActivity.loginDetailsEntered: Account " + inserted.getId() + " inserted.");
                            connectionCenter.createConnection(accountModel);
                            signinSuccessful.setValue(true);
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.e(TAG, "Could not insert new Account data.", e);
                        }
                    });

        }
    }
}
