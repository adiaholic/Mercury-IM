package org.mercury_im.messenger.ui.chat;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.jxmpp.jid.EntityBareJid;
import org.mercury_im.messenger.MercuryImApplication;
import org.mercury_im.messenger.core.centers.ConnectionCenter;
import org.mercury_im.messenger.persistence.entity.ChatModel;
import org.mercury_im.messenger.persistence.entity.ContactModel;
import org.mercury_im.messenger.persistence.entity.EntityModel;
import org.mercury_im.messenger.persistence.entity.MessageModel;
import org.mercury_im.messenger.persistence.repository.ChatRepository;
import org.mercury_im.messenger.persistence.repository.MessageRepository;
import org.mercury_im.messenger.persistence.repository.RosterRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class ChatViewModel extends ViewModel {

    private final CompositeDisposable disposable = new CompositeDisposable();

    @Inject
    MessageRepository messageRepository;

    @Inject
    RosterRepository rosterRepository;

    @Inject
    ChatRepository chatRepository;

    @Inject
    ConnectionCenter connectionCenter;

    private MutableLiveData<EntityModel> entity = new MutableLiveData<>();
    private MutableLiveData<ContactModel> contact = new MutableLiveData<>();
    private MutableLiveData<List<MessageModel>> messages = new MutableLiveData<>();
    private MutableLiveData<String> contactDisplayName = new MutableLiveData<>();
    private MutableLiveData<ChatModel> chat = new MutableLiveData<>();

    public ChatViewModel() {
        super();
        MercuryImApplication.getApplication().getAppComponent().inject(this);
    }

    public void init(long accountId, EntityBareJid jid) {
        disposable.add(rosterRepository.getOrCreateEntity(accountId, jid)
                .subscribe((Consumer<EntityModel>) this::init));
    }

    public void init(EntityModel entityModel) {

        disposable.add(rosterRepository.getContact(entityModel.getAccount().getId(), entityModel.getJid())
                .subscribe(reactiveResult -> {
                            ContactModel model = reactiveResult.first();
                            ChatViewModel.this.contact.setValue(model);
                            contactDisplayName.setValue(model.getRostername());
                        }));

        disposable.add(messageRepository.getAllMessagesOfEntity(entityModel)
        .subscribe(reactiveResult -> {
            List<MessageModel> messages = reactiveResult.toList();
            ChatViewModel.this.messages.setValue(messages);
        }));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.clear();
    }

    public LiveData<List<MessageModel>> getMessages() {
        return messages;
    }

    public LiveData<ContactModel> getContact() {
        return contact;
    }

    public LiveData<String> getContactDisplayName() {
        return contactDisplayName;
    }

    public void queryTextChanged(String query) {
        /*
        if (query.isEmpty()) {
            disposable.add(messageRepository.getAllMessagesOfChat(accountId, jid)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe((Consumer<List<MessageModel>>)
                            messages -> ChatViewModel.this.messages.setValue(messages)));
        }
        disposable.add(messageRepository.findMessageByQuery(accountId, jid, query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Consumer<List<MessageModel>>) o -> {
                    messages.setValue(o);
                }));

         */
    }

    public Completable requestMamMessages() {
        return Completable.fromAction(() -> {
            ChatModel chatModel = ChatViewModel.this.chat.getValue();
            if (chatModel == null) {
                return;
            }
            connectionCenter.requestMamMessagesFor(chatModel);
        });

    }
}
