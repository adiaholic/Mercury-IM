package org.mercury_im.messenger.ui.chatlist;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.mercury_im.messenger.MercuryImApplication;
import org.mercury_im.messenger.persistence.entity.ChatModel;
import org.mercury_im.messenger.persistence.repository.ChatRepository;
import org.mercury_im.messenger.persistence.repository.MessageRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class ChatListViewModel extends ViewModel {

    @Inject
    ChatRepository chatRepository;

    @Inject
    MessageRepository messageRepository;

    private CompositeDisposable disposable = new CompositeDisposable();

    private final MutableLiveData<List<ChatModel>> chats = new MutableLiveData<>();

    public ChatListViewModel() {
        MercuryImApplication.getApplication().getAppComponent().inject(this);

        disposable.add(chatRepository.getVisibleChats()
                .subscribe(result -> chats.setValue(result.toList())));
    }

    public LiveData<List<ChatModel>> getChats() {
        return chats;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.dispose();
    }
}
