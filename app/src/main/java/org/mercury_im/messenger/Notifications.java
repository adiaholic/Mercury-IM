package org.mercury_im.messenger;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import org.mercury_im.messenger.persistence.entity.ChatModel;
import org.mercury_im.messenger.ui.chat.ChatActivity;

public class Notifications {

    public static final String NOTIFICATION_CHANNEL__FOREGROUND_SERVICE = "foreground_service";
    public static final String NOTIFICATION_CHANNEL__NEW_MESSAGE = "new_message";
    public static final String NOTIFICATION_CHANNEL__INCOMING_CALL = "incoming_call"; // One day...
    public static final String NOTIFICATION_CHANNEL__DEBUG = "debug";

    // Notification IDs
    public static final int FOREGROUND_SERVICE_ID = 1; // must not be 0

    public static int chatMessageReceived(Context context, ChatModel chat, String contactName, String body) {
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);
        int id = (int) chat.getId();

        Intent tapAction = new Intent(context, ChatActivity.class);
        tapAction.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        tapAction.putExtra(ChatActivity.EXTRA_JID, chat.getPeer().getJid().toString());
        tapAction.putExtra(ChatActivity.EXTRA_ACCOUNT, chat.getPeer().getAccount().getId());
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, tapAction, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context,
                Notifications.NOTIFICATION_CHANNEL__NEW_MESSAGE);
        builder.setContentTitle(context.getResources().getString(R.string.notification_title, contactName));
        builder.setContentText(body);
        builder.setSmallIcon(R.drawable.ic_person_outline_black_24dp);
        builder.setContentIntent(pendingIntent);
        builder.setAutoCancel(true);

        notificationManagerCompat.notify(id, builder.build());

        return id;
    }

}
