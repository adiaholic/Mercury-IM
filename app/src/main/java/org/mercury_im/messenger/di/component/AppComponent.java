package org.mercury_im.messenger.di.component;

import org.mercury_im.messenger.MercuryImApplication;
import org.mercury_im.messenger.core.di.XmppComponent;
import org.mercury_im.messenger.core.stores.PlainMessageStore;
import org.mercury_im.messenger.di.module.AppModule;
import org.mercury_im.messenger.di.module.AndroidPersistenceModule;
import org.mercury_im.messenger.service.XmppConnectionService;
import org.mercury_im.messenger.ui.MainActivity;
import org.mercury_im.messenger.ui.chat.ChatActivity;
import org.mercury_im.messenger.ui.chat.ChatInputFragment;
import org.mercury_im.messenger.ui.chat.ChatInputViewModel;
import org.mercury_im.messenger.ui.chat.ChatViewModel;
import org.mercury_im.messenger.ui.chatlist.ChatListViewModel;
import org.mercury_im.messenger.ui.login.AccountsViewModel;
import org.mercury_im.messenger.ui.login.LoginActivity;
import org.mercury_im.messenger.ui.login.LoginViewModel;
import org.mercury_im.messenger.ui.roster.contacts.ContactListViewModel;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Main Application Component that binds together all the modules needed for the Android
 * application.
 */
@Singleton
@Component(
        modules = {
                AppModule.class,
                AndroidPersistenceModule.class
        })
public interface AppComponent {

    // Application

    void inject(MercuryImApplication mercuryImApplication);


    // Views

    void inject(MainActivity mainActivity);

    void inject(LoginActivity loginActivity);

    void inject(ChatActivity chatActivity);

    void inject(ChatInputFragment chatInputFragment);

    void inject(ChatListViewModel chatListViewModel);


    // ViewModels

    void inject(ContactListViewModel contactListViewModel);

    void inject(ChatViewModel chatViewModel);

    void inject(ChatInputViewModel chatInputViewModel);

    void inject(LoginViewModel loginViewModel);

    void inject(AccountsViewModel accountsViewModel);


    // Services

    void inject(XmppConnectionService service);


    // Connectors

    void inject(PlainMessageStore messageHandler);

}
