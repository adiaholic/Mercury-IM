package org.mercury_im.messenger.di.module;

import android.app.Application;

import dagger.Module;
import dagger.Provides;

import org.mercury_im.messenger.MercuryImApplication;
import org.mercury_im.messenger.core.NotificationManager;
import org.mercury_im.messenger.core.di.CenterModule;
import org.mercury_im.messenger.persistence.di.RequeryModule;

import javax.inject.Singleton;

@Module(includes = {
        CenterModule.class,
        RequeryModule.class
})
public class AppModule {

    private MercuryImApplication mApplication;

    public AppModule(MercuryImApplication application) {
        this.mApplication = application;
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    NotificationManager providerNotificationManager() {
        return mApplication;
    }
}
