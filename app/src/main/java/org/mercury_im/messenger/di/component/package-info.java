/**
 * The Component specifies which classes can be injected from the module.
 */
package org.mercury_im.messenger.di.component;
