package org.mercury_im.messenger;

import android.os.Parcel;
import android.os.Parcelable;

import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jxmpp.stringprep.XmppStringprepException;

public class ParcelableXMPPTCPConnectionConfiguration implements Parcelable {

    private final String username;
    private final String password;
    private final String xmppDomain;
    private final String resourcePart;
    private final String host;
    private final int port;

    private XMPPTCPConnectionConfiguration configuration;

    public static final Creator<ParcelableXMPPTCPConnectionConfiguration> CREATOR = new Creator<ParcelableXMPPTCPConnectionConfiguration>() {
        @Override
        public ParcelableXMPPTCPConnectionConfiguration createFromParcel(Parcel in) {
            return new ParcelableXMPPTCPConnectionConfiguration(in);
        }

        @Override
        public ParcelableXMPPTCPConnectionConfiguration[] newArray(int size) {
            return new ParcelableXMPPTCPConnectionConfiguration[size];
        }
    };

    public ParcelableXMPPTCPConnectionConfiguration(String username,
                                                    String password,
                                                    String xmppDomain,
                                                    String resourcePart,
                                                    String host,
                                                    int port) {
        this.username = username;
        this.password = password;
        this.xmppDomain = xmppDomain;
        this.resourcePart = resourcePart;
        this.host = host;
        this.port = port;
    }

    private ParcelableXMPPTCPConnectionConfiguration(Parcel in) {
        this(in.readString(),       // username
                in.readString(),    // password
                in.readString(),    // xmppDomain
                in.readString(),    // resourcePart
                in.readString(),    // host
                in.readInt());      // port
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(username);
        parcel.writeString(password);
        parcel.writeString(xmppDomain);
        parcel.writeString(resourcePart);
        parcel.writeString(host);
        parcel.writeInt(port);
    }

    public XMPPTCPConnectionConfiguration getConfiguration() throws XmppStringprepException {
        if (configuration != null) {
            return configuration;
        }

        XMPPTCPConnectionConfiguration.Builder builder = XMPPTCPConnectionConfiguration.builder();

        builder.setUsernameAndPassword(username, password);
        if (xmppDomain != null) builder.setXmppDomain(xmppDomain);
        if (resourcePart != null) builder.setResource(resourcePart);
        if (host != null) builder.setHost(host);
        if (port != -1) builder.setPort(port);

        configuration = builder.build();
        return configuration;
    }
}
