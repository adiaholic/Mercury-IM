package org.mercury_im.messenger;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mercury_im.messenger.core.connection.ConnectionState;

import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.TestScheduler;
import io.reactivex.subjects.BehaviorSubject;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }


    @Test
    public void test() {
        TestScheduler testScheduler = new TestScheduler();
        BehaviorSubject<ConnectionState> state = BehaviorSubject.createDefault(ConnectionState.DISCONNECTED);
        TestObserver<ConnectionState> observable = state.test();
        assertEquals(ConnectionState.DISCONNECTED, state.getValue());
        state.onNext(ConnectionState.CONNECTING);
        assertEquals(ConnectionState.CONNECTING, state.getValue());
        observable.assertValues(ConnectionState.DISCONNECTED, ConnectionState.CONNECTING).dispose();
    }
}