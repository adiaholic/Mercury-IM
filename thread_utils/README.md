The purpose of this module is to hold the `ThreadUtils` class,
on which both modules `persistence` as well as `core` depend.

By moving that class into a separate module, we prevent a circular 
dependency between `core` and `persistence`.