package org.mercury_im.messenger.persistence.repository;

import org.mercury_im.messenger.persistence.entity.EntityCapsModel;
import org.mercury_im.messenger.thread_utils.ThreadUtils;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.requery.Persistable;
import io.requery.reactivex.ReactiveEntityStore;
import io.requery.reactivex.ReactiveResult;

public class EntityCapsRepository extends AbstractRepository<EntityCapsModel> {

    @Inject
    public EntityCapsRepository(ReactiveEntityStore<Persistable> data,
                                @Named(value = ThreadUtils.SCHEDULER_IO) Scheduler subscriberScheduler,
                                @Named(value = ThreadUtils.SCHEDULER_UI) Scheduler observerScheduler) {
        super(EntityCapsModel.class, data, subscriberScheduler, observerScheduler);
    }
}
