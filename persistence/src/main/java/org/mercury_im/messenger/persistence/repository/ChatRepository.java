package org.mercury_im.messenger.persistence.repository;

import org.jxmpp.jid.EntityBareJid;
import org.mercury_im.messenger.persistence.entity.ChatModel;
import org.mercury_im.messenger.persistence.entity.ContactModel;
import org.mercury_im.messenger.persistence.entity.EntityModel;
import org.mercury_im.messenger.persistence.util.ChatAndPossiblyContact;
import org.mercury_im.messenger.thread_utils.ThreadUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.functions.Function;
import io.requery.Persistable;
import io.requery.query.Tuple;
import io.requery.reactivex.ReactiveEntityStore;
import io.requery.reactivex.ReactiveResult;

public class ChatRepository extends AbstractRepository<ChatModel> {

    private final Logger LOGGER = Logger.getLogger(ChatRepository.class.getName());

    @Inject
    public ChatRepository(ReactiveEntityStore<Persistable> data,
                          @Named(value = ThreadUtils.SCHEDULER_IO) Scheduler subscriberScheduler,
                          @Named(value = ThreadUtils.SCHEDULER_UI) Scheduler observerScheduler) {
        super(ChatModel.class, data, subscriberScheduler, observerScheduler);
    }

    public Observable<ReactiveResult<ChatModel>> getChatWith(EntityModel entityModel) {
        return getChatWith(entityModel.getAccount().getId(), entityModel.getJid());
    }

    public Observable<ReactiveResult<ChatModel>> getChatWith(long accountId, EntityBareJid jid) {
        return data().select(ChatModel.class).join(EntityModel.class).on(ChatModel.PEER_ID.eq(EntityModel.ID))
                .where(EntityModel.ACCOUNT_ID.eq(accountId).and(EntityModel.JID.eq(jid)))
                .get().observableResult()
                .doOnError(error -> LOGGER.log(Level.WARNING, "An error occurred while getting chat", error))
                .subscribeOn(subscriberScheduler())
                .observeOn(observerScheduler());
    }

    public Observable<ReactiveResult<ChatModel>> getVisibleChats() {
        return data().select(ChatModel.class).where(ChatModel.DISPLAYED.eq(true))
                .get().observableResult()
                .subscribeOn(subscriberScheduler())
                .observeOn(observerScheduler());
    }

    public Observable<List<ChatAndPossiblyContact>> getChatsAndContacts() {
        return Observable.fromCallable(() -> {
            List<ChatModel> chats = data().select(ChatModel.class)
                    .from(ChatModel.class).leftJoin(ContactModel.class).on(ChatModel.PEER_ID.eq(ContactModel.ENTITY_ID))
                    .get().toList();
            List<ChatAndPossiblyContact> chatsAndContacts = new ArrayList<>(chats.size());
            for (ChatModel chatModel : chats) {
                ContactModel contactModel = data().select(ContactModel.class).from(ContactModel.class).where(ContactModel.ENTITY_ID.eq(chatModel.getPeer().getId()))
                        .get().first();
                chatsAndContacts.add(new ChatAndPossiblyContact(chatModel, contactModel));
            }
            return chatsAndContacts;
        })
                .subscribeOn(subscriberScheduler())
                .observeOn(observerScheduler());
    }

    public Observable<ChatAndPossiblyContact> getChatAndContact(long accountId, EntityBareJid jid) {

        return Observable.fromCallable(() -> {
            ChatModel chat = data().select(ChatModel.class)
                    .from(ChatModel.class).join(EntityModel.class).on(ChatModel.PEER_ID.eq(EntityModel.ID))
                    .leftJoin(ContactModel.class).on(EntityModel.ID.eq(ContactModel.ENTITY_ID))
                    .where(EntityModel.ACCOUNT_ID.eq(accountId).and(EntityModel.JID.eq(jid)))
                    .get().firstOrNull();
            if (chat == null) {
                return null;
            }

            ContactModel contactModel = data().select(ContactModel.class).from(ContactModel.class)
                    .where(ContactModel.ENTITY_ID.eq(chat.getPeer().getId()))
                        .get().firstOrNull();
                return new ChatAndPossiblyContact(chat, contactModel);
        })
                .subscribeOn(subscriberScheduler())
                .observeOn(observerScheduler());
    }
}
