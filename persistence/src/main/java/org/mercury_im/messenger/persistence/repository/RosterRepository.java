package org.mercury_im.messenger.persistence.repository;

import org.jxmpp.jid.EntityBareJid;
import org.mercury_im.messenger.persistence.entity.AccountModel;
import org.mercury_im.messenger.persistence.entity.ContactModel;
import org.mercury_im.messenger.persistence.entity.EntityModel;
import org.mercury_im.messenger.thread_utils.ThreadUtils;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.requery.Persistable;
import io.requery.reactivex.ReactiveEntityStore;
import io.requery.reactivex.ReactiveResult;

public class RosterRepository extends RequeryRepository {

    @Inject
    public RosterRepository(ReactiveEntityStore<Persistable> data,
                            @Named(value = ThreadUtils.SCHEDULER_IO) Scheduler subscriberScheduler,
                            @Named(value = ThreadUtils.SCHEDULER_UI) Scheduler observerScheduler) {
        super(data, subscriberScheduler, observerScheduler);
    }

    /*
    ContactModel related methods
     */

    public Observable<ReactiveResult<ContactModel>> getAllContactsOfAccount(AccountModel accountModel) {
        return getAllContactsOfAccount(accountModel.getId());
    }

    public Observable<ReactiveResult<ContactModel>> getAllContactsOfAccount(long accountId) {
        return data().select(ContactModel.class).join(EntityModel.class).on(ContactModel.ENTITY_ID.eq(EntityModel.ID))
                .where(EntityModel.ACCOUNT_ID.eq(accountId))
                .get().observableResult()
                .subscribeOn(subscriberScheduler()).observeOn(observerScheduler());
    }

    public Single<ContactModel> upsertContact(ContactModel contact) {
        return data().upsert(contact).subscribeOn(subscriberScheduler()).observeOn(observerScheduler());
    }

    public Completable deleteContact(ContactModel contact) {
        return data().delete(contact).subscribeOn(subscriberScheduler()).observeOn(observerScheduler());
    }

    public Completable deleteContact(long accountId, EntityBareJid jid) {
        return data().delete(ContactModel.class).from(ContactModel.class)
                .join(EntityModel.class).on(ContactModel.ENTITY_ID.eq(EntityModel.ID))
                .where(EntityModel.ACCOUNT_ID.eq(accountId).and(EntityModel.JID.eq(jid)))
                .get().single().ignoreElement()
                .subscribeOn(subscriberScheduler()).observeOn(observerScheduler());
    }

    public Single<Integer> deleteAllContactsOfAccount(AccountModel account) {
        return deleteAllContactsOfAccount(account.getId());
    }

    public Single<Integer> deleteAllContactsOfAccount(long accountId) {
        return data().delete(ContactModel.class).from(ContactModel.class)
                .join(EntityModel.class).on(ContactModel.ENTITY_ID.eq(EntityModel.ID))
                .where(EntityModel.ACCOUNT_ID.eq(accountId))
                .get().single()
                .subscribeOn(subscriberScheduler()).observeOn(observerScheduler());
    }


    /*
    EntityModel related methods
     */

    public Observable<ReactiveResult<EntityModel>> getAllEntitiesOfAccount(AccountModel account) {
        return getAllEntitiesOfAccount(account.getId());
    }

    public Observable<ReactiveResult<EntityModel>> getAllEntitiesOfAccount(long accountId) {
        return data().select(EntityModel.class).where(EntityModel.ACCOUNT_ID.eq(accountId))
                .get().observableResult()
                .subscribeOn(subscriberScheduler())
                .observeOn(observerScheduler());
    }

    public Observable<ReactiveResult<EntityModel>> getEntityById(long entityId) {
        return data().select(EntityModel.class).where(EntityModel.ID.eq(entityId))
                .get().observableResult()
                .subscribeOn(subscriberScheduler())
                .observeOn(observerScheduler());
    }

    public Observable<ReactiveResult<EntityModel>> getEntityByJid(AccountModel account, EntityBareJid jid) {
        return getEntityByJid(account.getId(), jid);
    }

    public Observable<ReactiveResult<EntityModel>> getEntityByJid(long accountId, EntityBareJid jid) {
        return data().select(EntityModel.class)
                .where(EntityModel.ACCOUNT_ID.eq(accountId).and(EntityModel.JID.eq(jid)))
                .get().observableResult()
                .subscribeOn(subscriberScheduler())
                .observeOn(observerScheduler());
    }

    public Single<EntityModel> getOrCreateEntity(long accountId, EntityBareJid jid) {
        return Single.fromCallable(() -> {
            AccountModel account = data().select(AccountModel.class).where(AccountModel.ID.eq(accountId))
                    .get().first();
            return getOrCreateEntity(account, jid).blockingGet();
        })
                .observeOn(observerScheduler())
                .subscribeOn(subscriberScheduler());
    }

    public Single<EntityModel> getOrCreateEntity(AccountModel account, EntityBareJid jid) {
        return Single.fromCallable(() -> {
            EntityModel entity = data().select(EntityModel.class)
                    .where(EntityModel.ACCOUNT_ID.eq(account.getId()).and(EntityModel.JID.eq(jid)))
                    .get().firstOrNull();
            if (entity == null) {
                entity = new EntityModel();
                entity.setAccount(account);
                entity.setJid(jid);
                entity = data().insert(entity).blockingGet();
            }
            return entity;
        })
                .observeOn(observerScheduler())
                .subscribeOn(subscriberScheduler());

    }

    public Single<EntityModel> upsertEntity(EntityModel entity) {
        return data().upsert(entity)
                .subscribeOn(subscriberScheduler())
                .observeOn(observerScheduler());
    }

    public Completable deleteEntity(EntityModel entity) {
        return data().delete(entity)
                .subscribeOn(subscriberScheduler())
                .observeOn(observerScheduler());
    }

    /*
    RosterVersion related methods
     */

    public Single<Integer> updateRosterVersion(long accountId, String rosterVer) {
        return data().update(AccountModel.class).set(AccountModel.ROSTER_VERSION, rosterVer)
                .where(AccountModel.ID.eq(accountId))
                .get().single()
                .subscribeOn(subscriberScheduler())
                .observeOn(observerScheduler());
    }

    public Observable<String> getRosterVersion(AccountModel account) {
        return getRosterVersion(account.getId());
    }

    public Observable<String> getRosterVersion(long accountId) {
        return data().select(AccountModel.class).where(AccountModel.ID.eq(accountId))
                .get().observableResult()
                .subscribeOn(subscriberScheduler())
                .observeOn(observerScheduler())
                .map(accountModels -> {
                    AccountModel accountModel = accountModels.firstOrNull();
                    if (accountModel == null || accountModel.getRosterVersion() == null) {
                        return "";
                    }
                    return accountModel.getRosterVersion();
                });
    }

    public Single<AccountModel> updateRosterVersion(AccountModel account, String rosterVersion) {
        account.setRosterVersion(rosterVersion);
        return data().upsert(account)
                .subscribeOn(subscriberScheduler()).observeOn(observerScheduler());
    }

    public Observable<ReactiveResult<ContactModel>> getContact(AccountModel account, EntityBareJid jid) {
        return getContact(account.getId(), jid);
    }

    public Observable<ReactiveResult<ContactModel>> getContact(long accountId, EntityBareJid jid) {
        return data().select(ContactModel.class).from(ContactModel.class)
                .join(EntityModel.class).on(ContactModel.ENTITY_ID.eq(EntityModel.ID))
                .where(EntityModel.ACCOUNT_ID.eq(accountId).and(EntityModel.JID.eq(jid)))
                .get().observableResult()
                .subscribeOn(subscriberScheduler())
                .observeOn(observerScheduler());
    }

    public Observable<ReactiveResult<ContactModel>> getAllContacts() {
        return data().select(ContactModel.class)
                .get().observableResult()
                .subscribeOn(subscriberScheduler())
                .observeOn(observerScheduler());
    }
}
