package org.mercury_im.messenger.persistence.converter;

import org.mercury_im.messenger.persistence.enums.SubscriptionDirection;

import io.requery.Converter;

public class SubscriptionDirectionConverter implements Converter<SubscriptionDirection, String> {
    @Override
    public Class<SubscriptionDirection> getMappedType() {
        return SubscriptionDirection.class;
    }

    @Override
    public Class<String> getPersistedType() {
        return String.class;
    }

    @Override
    public Integer getPersistedSize() {
        return null;
    }

    @Override
    public String convertToPersisted(SubscriptionDirection subscriptionDirection) {
        return subscriptionDirection == null ? null : subscriptionDirection.name();
    }

    @Override
    public SubscriptionDirection convertToMapped(Class<? extends SubscriptionDirection> aClass, String string) {
        return string == null ? null : SubscriptionDirection.valueOf(string);
    }
}
