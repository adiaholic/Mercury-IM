package org.mercury_im.messenger.persistence.entity;

import org.jxmpp.jid.EntityBareJid;
import org.mercury_im.messenger.persistence.converter.EntityBareJidConverter;

import io.requery.Column;
import io.requery.Convert;
import io.requery.Entity;
import io.requery.Generated;
import io.requery.Key;
import io.requery.ManyToOne;
import io.requery.Persistable;
import io.requery.Table;

@Entity
@Table(name = "entities")
public abstract class AbstractEntityModel implements Persistable {

    @Key @Generated
    long id;

    @ManyToOne
    AccountModel account;

    @Convert(EntityBareJidConverter.class)
    @Column(nullable = false)
    EntityBareJid jid;

    @Override
    public String toString() {
        return "Entity[" + id + ", " +
                jid + ", " +
                account + "]";
    }
}
