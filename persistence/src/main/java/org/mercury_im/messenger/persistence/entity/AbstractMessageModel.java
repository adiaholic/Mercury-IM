package org.mercury_im.messenger.persistence.entity;

import org.jxmpp.jid.EntityBareJid;
import org.mercury_im.messenger.persistence.converter.EntityBareJidConverter;

import java.util.Date;

import io.requery.Column;
import io.requery.Convert;
import io.requery.Entity;
import io.requery.ForeignKey;
import io.requery.Generated;
import io.requery.Key;
import io.requery.ManyToOne;
import io.requery.Persistable;
import io.requery.Table;

@Entity
@Table(name = "messages")
public abstract class AbstractMessageModel implements Persistable {

    @Key @Generated
    long id;

    @ForeignKey(referencedColumn = "id")
    @ManyToOne
    ChatModel chat;

    String body;

    @Column(name = "\"timestamp\"", nullable = false)
    Date timestamp;

    @Convert(EntityBareJidConverter.class)
    @Column(nullable = false)
    EntityBareJid sender;

    @Convert(EntityBareJidConverter.class)
    @Column(nullable = false)
    EntityBareJid recipient;

    boolean incoming;

    String thread;

    @Column(nullable = false)
    String legacyId;

    String originId;

    String stanzaId;

    @Convert(EntityBareJidConverter.class)
    EntityBareJid stanzaIdBy;
}
