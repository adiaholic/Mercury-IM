package org.mercury_im.messenger.persistence.entity;

import io.requery.Entity;
import io.requery.ForeignKey;
import io.requery.Generated;
import io.requery.Key;
import io.requery.OneToOne;
import io.requery.Persistable;
import io.requery.Table;

@Entity
@Table(name = "chats")
public abstract class AbstractChatModel implements Persistable {

    @Key @Generated
    long id;

    @OneToOne
    @ForeignKey
    EntityModel peer;

    boolean displayed;
}
