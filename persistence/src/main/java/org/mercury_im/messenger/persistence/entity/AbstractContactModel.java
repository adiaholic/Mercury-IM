package org.mercury_im.messenger.persistence.entity;

import org.mercury_im.messenger.persistence.converter.SubscriptionDirectionConverter;
import org.mercury_im.messenger.persistence.enums.SubscriptionDirection;

import io.requery.Convert;
import io.requery.Entity;
import io.requery.ForeignKey;
import io.requery.Generated;
import io.requery.Key;
import io.requery.OneToOne;
import io.requery.Persistable;
import io.requery.Table;

@Entity
@Table(name = "contacts")
public abstract class AbstractContactModel implements Persistable {

    @Key @Generated
    long id;

    @OneToOne
    @ForeignKey(referencedColumn = "id")
    EntityModel entity;

    String rostername;

    @Convert(SubscriptionDirectionConverter.class)
    SubscriptionDirection sub_direction;

    boolean sub_pending;

    boolean sub_approved;

    @Override
    public String toString() {
        return "Contact[" + id + ", " +
                rostername + ", " +
                entity + ", " +
                sub_direction + ", " +
                (sub_pending ? "pending" : "not pending") + ", " +
                (sub_approved ? "approved" : "not approved") + "]";
    }
}
