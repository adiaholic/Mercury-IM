package org.mercury_im.messenger.persistence.repository;

import org.mercury_im.messenger.persistence.entity.ChatModel;
import org.mercury_im.messenger.persistence.entity.ContactModel;
import org.mercury_im.messenger.persistence.entity.EntityModel;
import org.mercury_im.messenger.persistence.entity.MessageModel;
import org.mercury_im.messenger.thread_utils.ThreadUtils;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.requery.Persistable;
import io.requery.reactivex.ReactiveEntityStore;
import io.requery.reactivex.ReactiveResult;

public class MessageRepository extends AbstractRepository<MessageModel> {

    @Inject
    public MessageRepository(ReactiveEntityStore<Persistable> data,
                             @Named(value = ThreadUtils.SCHEDULER_IO) Scheduler subscriberScheduler,
                             @Named(value = ThreadUtils.SCHEDULER_UI) Scheduler observerScheduler) {
        super(MessageModel.class, data, subscriberScheduler, observerScheduler);
    }

    public Observable<ReactiveResult<MessageModel>> getAllMessagesOfChat(ChatModel chat) {
        return getAllMessagesOfChat(chat.getId());
    }

    public Observable<ReactiveResult<MessageModel>> getAllMessagesOfChat(long chatId) {
        return data().select(MessageModel.class).where(MessageModel.CHAT_ID.eq(chatId))
                .orderBy(MessageModel.TIMESTAMP.asc())
                .get().observableResult()
                .subscribeOn(subscriberScheduler())
                .observeOn(observerScheduler());
    }

    public Observable<ReactiveResult<MessageModel>> getAllMessagesOfEntity(EntityModel entity) {
        return getAllMessagesOfEntity(entity.getId());
    }

    public Observable<ReactiveResult<MessageModel>> getAllMessagesOfEntity(long entityId) {
        return data().select(MessageModel.class).from(MessageModel.class)
                .join(ChatModel.class).on(MessageModel.CHAT_ID.eq(ChatModel.ID))
                .where(ChatModel.PEER_ID.eq(entityId))
                .orderBy(MessageModel.TIMESTAMP.asc())
                .get().observableResult()
                .subscribeOn(subscriberScheduler())
                .observeOn(observerScheduler());
    }

    public Observable<ReactiveResult<MessageModel>> getAllMessagesOfContact(ContactModel contact) {
        return getAllMessagesOfContact(contact.getId());
    }

    public Observable<ReactiveResult<MessageModel>> getAllMessagesOfContact(long contactId) {
        return data().select(MessageModel.class).join(ChatModel.class).on(MessageModel.CHAT_ID.eq(ChatModel.ID))
                .join(EntityModel.class).on(ChatModel.PEER_ID.eq(EntityModel.ID))
                .join(ContactModel.class).on(EntityModel.ID.eq(ContactModel.ENTITY_ID))
                .orderBy(MessageModel.TIMESTAMP.asc())
                .get().observableResult()
                .subscribeOn(subscriberScheduler())
                .observeOn(observerScheduler());
    }
}
