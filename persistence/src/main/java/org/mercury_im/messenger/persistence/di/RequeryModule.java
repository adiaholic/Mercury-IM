package org.mercury_im.messenger.persistence.di;

import org.mercury_im.messenger.persistence.repository.AccountRepository;
import org.mercury_im.messenger.persistence.repository.ChatRepository;
import org.mercury_im.messenger.persistence.repository.EntityCapsRepository;
import org.mercury_im.messenger.persistence.repository.RosterRepository;
import org.mercury_im.messenger.thread_utils.ThreadUtils;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.requery.Persistable;
import io.requery.reactivex.ReactiveEntityStore;

@Module
public class RequeryModule {

    @Provides
    @Singleton
    public static AccountRepository provideAccountRepository(ReactiveEntityStore<Persistable> data,
                                                      @Named(value = ThreadUtils.SCHEDULER_IO) Scheduler ioScheduler,
                                                      @Named(value = ThreadUtils.SCHEDULER_UI) Scheduler uiScheduler) {
        return new AccountRepository(data, ioScheduler, uiScheduler);
    }

    @Provides
    @Singleton
    public static ChatRepository provideChatRepository(ReactiveEntityStore<Persistable> data,
                                                       @Named(value = ThreadUtils.SCHEDULER_IO) Scheduler ioScheduler,
                                                       @Named(value = ThreadUtils.SCHEDULER_UI) Scheduler uiScheduler) {
        return new ChatRepository(data, ioScheduler, uiScheduler);
    }

    @Provides
    @Singleton
    public static EntityCapsRepository provideCapsRepository(ReactiveEntityStore<Persistable> data,
                                                             @Named(value = ThreadUtils.SCHEDULER_IO) Scheduler ioScheduler,
                                                             @Named(value = ThreadUtils.SCHEDULER_UI) Scheduler uiScheduler) {
        return new EntityCapsRepository(data, ioScheduler, uiScheduler);
    }

    @Provides
    @Singleton
    public static RosterRepository provideRosterRepository(ReactiveEntityStore<Persistable> data,
                                                           @Named(value = ThreadUtils.SCHEDULER_IO) Scheduler ioScheduler,
                                                           @Named(value = ThreadUtils.SCHEDULER_UI) Scheduler uiScheduler) {
        return new RosterRepository(data, ioScheduler, uiScheduler);
    }
}
