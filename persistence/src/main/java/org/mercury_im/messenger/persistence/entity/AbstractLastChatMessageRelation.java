package org.mercury_im.messenger.persistence.entity;

import io.requery.Entity;
import io.requery.ForeignKey;
import io.requery.Key;
import io.requery.OneToOne;
import io.requery.Persistable;
import io.requery.Table;

@Entity
@Table(name = "last_messages")
public abstract class AbstractLastChatMessageRelation implements Persistable {

    @Key
    @OneToOne
    @ForeignKey
    ChatModel chat;

    @OneToOne
    @ForeignKey
    MessageModel message;
}
