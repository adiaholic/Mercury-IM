package org.mercury_im.messenger.persistence.util;

import org.mercury_im.messenger.persistence.entity.ChatModel;
import org.mercury_im.messenger.persistence.entity.ContactModel;

public class ChatAndPossiblyContact {

    private final ChatModel chat;
    private final ContactModel contact;

    public ChatAndPossiblyContact(ChatModel chat, ContactModel contact) {
        this.chat = chat;
        this.contact = contact;
    }

    public ChatModel getChat() {
        return chat;
    }

    public ContactModel getContact() {
        return contact;
    }
}
