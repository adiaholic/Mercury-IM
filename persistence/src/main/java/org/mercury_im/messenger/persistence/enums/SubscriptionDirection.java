package org.mercury_im.messenger.persistence.enums;

public enum  SubscriptionDirection {
    none,
    to,
    from,
    both,
    remove
}
