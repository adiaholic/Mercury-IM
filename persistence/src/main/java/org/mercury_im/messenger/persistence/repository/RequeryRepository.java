package org.mercury_im.messenger.persistence.repository;

import io.reactivex.Scheduler;
import io.requery.Persistable;
import io.requery.reactivex.ReactiveEntityStore;

public abstract class RequeryRepository {

    private final Scheduler subscriberScheduler;
    private final Scheduler observerScheduler;

    private final ReactiveEntityStore<Persistable> data;

    protected RequeryRepository(ReactiveEntityStore<Persistable> data,
                                Scheduler subscriberScheduler,
                                Scheduler observerScheduler) {
        this.data = data;
        this.subscriberScheduler = subscriberScheduler;
        this.observerScheduler = observerScheduler;
    }

    protected ReactiveEntityStore<Persistable> data() {
        return data;
    }

    protected Scheduler subscriberScheduler() {
        return subscriberScheduler;
    }

    protected Scheduler observerScheduler() {
        return observerScheduler;
    }

}
